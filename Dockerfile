FROM anapsix/alpine-java
LABEL maintainer="devops@vibedesenv.com"
COPY target/produto-0.0.2-SNAPSHOT.jar /home/produto-0.0.2-SNAPSHOT.jar
CMD ["java","-Xmx128m","-jar", "-Dspring.profiles.active=cloud","/home/produto-0.0.2-SNAPSHOT.jar"]